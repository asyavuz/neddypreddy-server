<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="X-Frame-Options" content="SAMEORIGIN">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Neddylation Site Predictor | NeddyPreddy</title>
    <meta name="description" content="NeddyPreddy predicts protein neddylation sites and gives detailed reports." />
    <meta name="keywords" content="Neddylation, Nedd8, NeddyPreddy, Rub1, Rubylation, Prediction, Predictor, Web Server, Sabanci University, Sezerman Lab" />
    <link href="css/main.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
    <script src="js/jquery.min.js"></script>
    <script src="js/main.min.js"></script>
    <script type='text/javascript'>
  function get_sample() {
    var v=document.getElementById('sampleInput');
    var s='>sp|P62805|H4_HUMAN Histone H4\n';
    s=s+'MSGRGKGGKGLGKGGAKRHRKVLRDNIQGITKPAIRRLARRGGVKRISGLIYEETRG\n';
    s=s+'VLKVFLENVIRDAVTYTEHAKRKTVTAMDVVYALKRQGRTLYGFGG\n';
     s=s+'>sp|P04908|H2A1B_HUMAN Histone H2A type 1-B/E\n';
    s=s+'MSGRGKQGGKARAKAKTRSSRAGLQFPVGRVHRLLRKGNYSERVGAGAPVYLAAV\n';
    s=s+'LEYLTAEILELAGNAARDNKKTRIIPRHLQLAIRNDEELNKLLGRVTIAQGGVLP\n';
    s=s+'NIQAVLLPKKTESHHKAKGK\n';
    v.value=s;
    }
    </script>
    <script>
    function resetform() {
      document.getElementById("submit-form").reset();
      }
    </script>

  </head>
  <body>
      <div class="container-fluid">
    <div class="row" id="header">
      <div class="col-md-1"></div>
      <div class="col-md-4" align="center" id="logo"><img src="images/neddypreddy-logo.png" height=250px ></img></div>
      <div class="col-md-5">
        <h1 align="center">NeddyPreddy</h1>
        <h4 align="center">Neddylation is a post-translational modification,<br> when distrupted it may cause in serious diseases <br>such as Alzheimer's and cancer.<br><br> NeddyPreddy predicts protein neddylation <br>sites and gives detailed reports.
      </div>
    </div>
    <div class="container-fluid" id="input">
    <div class="row">
      <div class="col-md-12"><h2 align="center">Submit Your Sequences</h2></div>
      <div class="col-md-3"></div>
      <div class="col-md-6" align="center">
      <h4>NeddyPreddy works with FASTA format</h4>NeddyPreddy uses FASTA format to work on neddylation sites.
      <br>You can submit sequences by using one of the options below.
      <br>
    </div>
  </div>
  <div class="row">
      <form method="post" id="submit-form" action="/cgi-bin/new_job.py" enctype="multipart/form-data">
        <div class="col-md-2"></div>
        <div class="col-md-8">
<div role="tabpanel">
  <ul class="nav nav-pills" role="tablist">
    <li role="presentation" class="active"><a href="#manual" aria-controls="manual" role="tab" data-toggle="tab" onclick="reset_form()">
      <p align="center"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span> Manual<br><h6>enter your sequences (FASTA or Uniprot IDs)</h6></a></li>
      <li role="presentation"><a id="#upload-button" href="#upload" aria-controls="upload" role="tab" data-toggle="tab">
         <p align="center"><span class="glyphicon glyphicon-upload" aria-hidden="true"></span> Upload<br><h6>upload a file for your FASTA sequences</h6></a></li>
    <li role="presentation"><a href="#sample" aria-controls="sample" role="tab" data-toggle="tab" onclick="get_sample()">
      <p align="center"><span class="glyphicon glyphicon-align-left" aria-hidden="true"></span> Sample<br><h6>generate sample sequences</h6></a></li>
  </ul>
  <br>
  <div class="tab-content" align="center">
    <div role="tabpanel" class="tab-pane fade in active" id="manual">
      <textarea class="form-control" rows="10" id="sequenceInput" name="sequenceInput" placeholder="Please enter your FASTA sequence(s) or Uniprot ID(s) (one per line)"></textarea>
    </div>
    <div role="tabpanel" class="tab-pane fade" id="upload">
      <div id="file-selection-box">
        <p>To select your FASTA file, please click <b>browse</b> button.</p>
        <div class="input-group">
                <span class="input-group-btn">
                    <span class="btn btn-primary-table btn-file">
                        Browse <input type="file" name="file">
                    </span>
                </span>
                <input type="text" class="form-control" readonly placeholder="No file is selected">
            </div>
</span>
</div>
    </div>
    <div role="tabpanel" class="tab-pane" id="sample">
      <textarea class="form-control" rows="10" id="sampleInput" name="sampleInput"></textarea>
    </div>
  </div>
</div>
</div>
    </div>
        <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
        <div class="form-group" align="center">
          <div class="control-group">
          <label class="sr-only" for="email">Enter E-mail Address</label>
          <div class="controls">
          <input type="email" class="form-control" name="email" id="email" placeholder="Please enter your e-mail address (optional)">
          <div class="form-actions">
          <input type="submit" class="btn btn-primary-table" role="button"></a>
          <input type="reset" class="btn btn-primary-table" role="button" id="reset" name="reset"></a>
        </div>
        </div>
        </div>
        </div>
      </div>
    </div>
    </form>
  </div>
</div>
<div class="container-fluid">
<div class="row" id="footer">
  <div class="col-md-1"></div>
  <div class="col-md-4"><h2>Citation information</h2><p>If you would like to use NeddyPreddy in your publications, please consider citing: <br>Yavuz A.S., Sozer N.B., Sezerman O.U. "NeddyPreddy: prediction of neddylation sites from protein sequences", in preparation.</p>
</div>
  <div class="col-md-4"><br>NeddyPreddy is developed and maintained at Sabancı University. Usage of NeddyPreddy is free for academic users.
    <br><br>For commercial use and any questions and suggestions please contact
    <a href="mailto:ugur.sezerman@acibadem.edu.tr" class="mail">ugur.sezerman@acibadem.edu.tr</a>
    <br><br><a class="btn btn-primary btn-lg" href="documentation.html" role="button">Documentation and FAQ</a></div>
<br>
  <div class="col-md-3"><a href="http://fens.sabanciuniv.edu"><img src="images/sabanci-logo.jpg"></img></a>
    <br>
    <a href="http://www.sezermanlab.org"><img src="images/sezermanlab-logo.jpg"></img></a>
  </div>
  <div class="row" id="footer">
    <div class="col-md-12"><br><br><p align="center">NeddyPreddy 2016</p></div>
</div>
</div>
</div>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-57123838-1', 'auto');
      ga('send', 'pageview');
    </script>
<script>
$(document).on('change', '.btn-file :file', function() {
  var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', [numFiles, label]);
});

$(document).ready( function() {
    $('.btn-file :file').on('fileselect', function(event, numFiles, label) {

        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;

        if( input.length ) {
            input.val(log);
        } else {
            if( log ) alert(log);
        }

    });
});
</script>
<script>
$( "#manual" ).click(function() {
  var v=document.getElementById('sampleInput');
  var s='';
  v.value=s;
});
</script>
<script>
$( "#upload" ).click(function() {
  var v=document.getElementById('sampleInput');
  var s='';
  v.value=s;
});
</script>
  </body>
</html>
