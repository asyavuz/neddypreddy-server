#!/disk2/shared/soft/bin/python
__author__ = 'asyavuz'
import os
import re
import sys
import cgi
import json
import cgitb
import shutil
import random
import string
import smtplib
import urllib2
import logging
import datetime
import mysql.connector
from Bio import SeqIO
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
try:
    config = json.load(open('config.json'))
except Exception, e:
    sys.stderr.write(str(e)+'\n')
    raise e

cgitb.enable()

def send_email(email, subject, message, cc=""):
    '''
    E-mail sending helper function
    :param jobid: ID of job
    :param email: Recipient e-mail address
    :param subject: E-mail subject
    :param message: Message of email
    :param cc: Carbon-copy recipient (if needed)
    :return: Nothing
    '''
    logger = logging.getLogger('email')

    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject
    msg['From'] = config['email']['from']
    msg['To'] = email
    if cc:
        msg['Cc'] = cc
        email = email+', '+cc

    plainmsg = 'Dear User,\n\n'+message+' \n\n'
    plainmsg = plainmsg.replace('<br />', '\n')
    plainmsg = re.sub(r'<a href=".*?">(.*?)</a>', r'\1', plainmsg)

    htmlmessage = """\
    <html>
      <head></head>
      <body>
        <p>Dear User,<br /><br />
            %s
        </p>
      </body>
    </html>
    """ % message

    # Record the MIME types of both parts - text/plain and text/html.
    part1 = MIMEText(plainmsg, 'plain')
    part2 = MIMEText(htmlmessage, 'html')

    # Attach parts into message container.
    # According to RFC 2046, the last part of a multipart message, in this case
    # the HTML message, is best and preferred.
    msg.attach(part1)
    msg.attach(part2)
    if config['email']['provider'] == 'smtp':
        logger.info("Sending email with subject: %s to: %s..." % (subject, email))
        user = config['email']['smtp']['username']
        pwd = config['email']['smtp']['password']

        try:
            smtpserver = smtplib.SMTP(config['email']['smtp']['host'], config['email']['smtp']['port'])
            smtpserver.ehlo()
            smtpserver.starttls()
            smtpserver.ehlo()
            smtpserver.login(user, pwd)
            smtpserver.sendmail(user, email, msg.as_string())
            smtpserver.close()
        except BaseException as e1:
            logger.error("Problem in sending email with SMTP. Exception message: %s." % str(e1))
            if config['email']['smtp']['fallback'] == 'sendmail':
                try:
                    sendmail = config['email']['sendmail']['executable']
                    header = 'To:' + email + '\n' + 'From: ' + config['email']['from'] + '\n' + 'Subject:'+subject+' \n'

                    p = os.popen("%s -t -i" % sendmail, "w")
                    p.write(header+'\n'+plainmsg)
                    status = p.close()
                    if status:
                        raise Exception("Sendmail exit status"+str(status))
                except BaseException as e:
                    logger.error("Problem in sending email with sendmail. Exception message: %s." % str(e))
                    raise(e)

    elif config['email']['provider'] == 'sendmail':
        try:
            sendmail = config['email']['sendmail']['executable']
            header = 'To:' + email + '\n' + 'From: ' + config['email']['from'] + '\n' + 'Subject:'+subject+' \n'

            p = os.popen("%s -t -i" % sendmail, "w")
            p.write(header+'\n'+plainmsg)
            status = p.close()
            if status:
                raise Exception("Sendmail exit status"+str(status))
        except BaseException as e:
            logger.error("Problem in sending email with sendmail. Exception message: %s." % str(e))
            raise(e)

logging.basicConfig(filename=config['server']['logDir']+'/submission.log', format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.DEBUG)

ifh = open('./templates/error.html')
error_template = ifh.read()
ifh.close()
ifh = open('./templates/in-progress.html')
inprogress_template = ifh.read()
ifh.close()

jobs_folder = config['server']['jobsDir']

# NeddyPreddy v1.0
def redirect():
        print 'Content-Type: text/html'
        print # HTTP says you have to have a blank line between headers and content
        print '<html>'
        print '  <head>'
        print '    <meta http-equiv="X-Frame-Options" content="SAMEORIGIN">'
        print '    <meta http-equiv="refresh" content="0;url=/jobs/%s/" />' %  jobid
        print '    <title>You are going to be redirected</title>'
        print '  </head>'
        print '  <body>'
        print '    Redirecting... <a href="/jobs/%s/">Click here if you are not redirected</a>' % jobid
        print '  </body>'
        print '</html>'

class InvalidAlphabet(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

class NoValidFASTA(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

class InvalidUniprotID(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

class ProblemUniprot(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

logger = logging.getLogger('submission')
queue = ""
# Create instance of FieldStorage
form = cgi.FieldStorage()
# Get data from fields
if form.getvalue('sequenceInput'):
    fasta = form.getvalue('sequenceInput')
    fasta.replace('\r\n', '\n')
    if fasta.startswith('queue=priority'):
        fasta.replace('queue=priority', '')
        queue = "priority"
        logger.info("Identified priority submission!")
    else:
        queue = "normal"
else:
    fasta = ""

if fasta == "" and form.getvalue('sampleInput'):
    fasta = form.getvalue('sampleInput')

if form.getvalue('email'):
    email = form.getvalue('email')
else:
    email = ""

if form.getvalue('file'):
    filecont = form.getvalue('file')
else:
    filecont = ""

complete = ""
if filecont and fasta:
    complete = filecont+'\n'+fasta
elif fasta:
    complete = fasta
elif filecont:
    complete = filecont

if not queue:
    queue = "normal"

exists = False
while (not exists):
    jobid = id_generator(15)
    if not os.path.exists(jobs_folder+jobid):
        os.mkdir(jobs_folder+jobid)
        exists = True

ofh = open(jobs_folder+jobid+'/'+jobid+'.fasta', 'w')
ofh.write(complete)
ofh.close()
ip = cgi.escape(os.environ["REMOTE_ADDR"])
try:
    cnx = mysql.connector.connect(user=config['mysql']['username'], password=config['mysql']['password'],
                                  host=config['mysql']['host'],
                                  database=config['mysql']['database'])
except Exception, e:
    send_email('neddypreddy@gmail.com', 'Incident: Job ID - %s, mySQL Related Problem!' % jobid,
                     '<br /> or <br /><br />Dear Admin,<br />Your server configuration is not working properly at the '
                     'moment. We have received following error: %s. <br /><br />Please fix this problem as soon as '
                     'possible.<br /><br />Submitter IP: %s<br /><br />Sincerely,<br />NeddyPreddy Server' % (str(e), str(ip)))

    ofh = open(jobs_folder+jobid+'/index.html', 'w')
    ofh.write(error_template.replace("<!--ERRORTEXT-->", "There is a problem with database connection at the moment. "
                                                         "Please try again later.<br />This error has been reported to "
                                                         "the NeddyPreddy team. <br />We apologize for any "
                                                         "inconvenience this may cause."))
    ofh.close()
    ofh = open(jobs_folder+jobid+'/ERROR.txt', 'w')
    ofh.close()
    sys.exit(1)

seqlen = 0
try:
    ifh = open(jobs_folder+jobid+'/'+jobid+'.fasta', 'r')
    wanted = set("ARNDCQEGHILKMFPSTWYV")
    rec = None
    seqs = list(SeqIO.parse(ifh, "fasta"))
    seqlen = len(seqs)
    logger.info("Submission %s contains %d FASTA-formatted sequences."%(jobid,seqlen))
    if len(seqs) == 0: # NO FASTA FORMATTED FILE. Uniprot ID maybe?
        seqs = []
        ifh.seek(0)
        lines = ifh.readlines()
        for line in lines:
            try:
                response = urllib2.urlopen('http://www.uniprot.org/uniprot/%s.fasta' % line.strip())
                seq = SeqIO.read(response, format='fasta')
                seq.id = line.strip()
                seqs.append(seq)
            except (urllib2.HTTPError, urllib2.URLError) as e:
                if str(e).startswith('HTTP Error 404'):
                    raise InvalidUniprotID(line.strip())
                else:
                    raise ProblemUniprot("Cannot reach to Uniprot for identifier %s at the moment. Received: %s. Sorry about that." % (line.strip(), str(e)))
    if len(seqs) == 0:
        raise NoValidFASTA("No valid FASTA sequence found!")
    final_seqs = []
    for record in seqs :
        if len(record.seq) < 10: # Do not support short peptides
                continue
        if not wanted.issuperset(record.seq):
            raise InvalidAlphabet(record.id)
        final_seqs.append(record)
    ifh.close()
    
    seqlen = len(final_seqs)
    ofh = open(jobs_folder+jobid+'/'+jobid+'.fasta', 'w')
    for seq in seqs:
        ofh.write(seq.format('fasta')+'\n')
    ofh.close()

    if seqlen == 0:
        raise NoValidFASTA("No valid FASTA-sequence found!")
    logger.info("Final sequence count for job %s is %d." % (jobid, seqlen))
except InvalidAlphabet as e:
    ofh = open(jobs_folder+jobid+'/index.html', 'w')
    ofh.write(error_template.replace("<!--ERRORTEXT-->", "Only single amino acid alphabet without ambiguity codes is supported. <br />Incompatible character found in <b>%s</b>!" % str(e)))
    ofh.close()
    ofh = open(jobs_folder+jobid+'/ERROR.txt', 'w')
    ofh.close()
    cnx.close()
    redirect()
    sys.exit(1)
except NoValidFASTA as e:
    ofh = open(jobs_folder+jobid+'/index.html', 'w')
    ofh.write(error_template.replace("<!--ERRORTEXT-->", "No valid FASTA-formatted sequence found in your input."))
    ofh.close()
    ofh = open(jobs_folder+jobid+'/ERROR.txt', 'w')
    ofh.close()
    cnx.close()
    redirect()
    sys.exit(1)
except InvalidUniprotID as e:
    ofh = open(jobs_folder+jobid+'/index.html', 'w')
    ofh.write(error_template.replace("<!--ERRORTEXT-->", "You have provided an invalid Uniprot ID: %s." % str(e)))
    ofh.close()
    ofh = open(jobs_folder+jobid+'/ERROR.txt', 'w')
    ofh.close()
    cnx.close()
    redirect()
    sys.exit(1)
except ProblemUniprot as e:
    ofh = open(jobs_folder+jobid+'/index.html', 'w')
    ofh.write(error_template.replace("<!--ERRORTEXT-->", str(e)))
    ofh.close()
    ofh = open(jobs_folder+jobid+'/ERROR.txt', 'w')
    ofh.close()
    cnx.close()
    redirect()
    sys.exit(1)
else:

    # If it came through here, lets fix the permissions for the runner and add the job to the queue
    os.chmod(jobs_folder+jobid, 0777)
    for root, dirs, files in os.walk(jobs_folder+jobid):
        for f in files:
            os.chmod(os.path.join(root, f), 0777)

    cursor = cnx.cursor()
    ip = cgi.escape(os.environ["REMOTE_ADDR"])
    submitdate = datetime.datetime.now()
    status = 0
    add_job = ("INSERT INTO queue (email, jobid, proteincount, submitdate, ip, status, queue) "
               "VALUES (%s, %s, %s, %s, %s, %s, %s); ")
    job_data = (email, jobid, seqlen, submitdate, ip, status, queue)
    cursor.execute(add_job, job_data)
    cnx.commit()

    joburl = "http://%s/jobs/%s/" % (os.environ['SERVER_NAME'], jobid)
    inprogress_template = inprogress_template.replace("<!--MYSQL_HOST-->", str(config['mysql']['host']))
    inprogress_template = inprogress_template.replace("<!--MYSQL_USER-->", str(config['mysql']['username']))
    inprogress_template = inprogress_template.replace("<!--MYSQL_PASS-->", str(config['mysql']['password']))
    inprogress_template = inprogress_template.replace("<!--DATABASE-->", str(config['mysql']['database']))
    inprogress_template = inprogress_template.replace("<!--ADMIN_EMAIL-->", str(config['server']['adminEmail']))
    inprogress_template = inprogress_template.replace("<!--JOBSDIR-->", str(config['server']['jobsDir']))
    inprogress_template = inprogress_template.replace("<!--JOBID-->", str(jobid))
    inprogress_template = inprogress_template.replace("<!--RESULTSURL-->", joburl)
    inprogress_template = inprogress_template.replace("<!--CITATION_INFO-->", str(config['server']['citation']))
    ofh = open(jobs_folder+jobid+'/index.php', 'w')
    ofh.write(inprogress_template)
    ofh.close()

    cursor.close()
    cnx.close()
    if email:
        send_email(email, "Job Queued: %s" % jobid, "Your job with ID %s has successfully been queued. "
                                                                 "You can track your submission at "
                                                                 "<a href=\"%s\">%s</a>.<br /><br />Sincerely,"
                                                                 "<br />NeddyPreddy "
                                                                 "Server<br />" % (jobid, joburl, joburl))
    redirect()



