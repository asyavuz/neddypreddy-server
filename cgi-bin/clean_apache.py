#!/disk2/shared/soft/bin/python
__author__ = 'asyavuz'
import os
import sys
import cgi
import json
import cgitb
import shutil
import logging

try:
    config = json.load(open('config.json'))
except Exception, e:
    sys.stderr.write(str(e)+'\n')
    raise e

logging.basicConfig(filename=config['server']['logDir']+'/cleaner_apache.log',
                                        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                                        level=logging.DEBUG)
mainLogger = logging.getLogger("cleaner")


cgitb.enable()

jobsdir = config['server']['jobsDir']
request_ip = cgi.escape(os.environ["REMOTE_ADDR"])
server_ip = str(config['server']['serverIP']
body = {"status":"Fail", "job":"N/A"}

if str(request_ip) != server_ip:
    mainLogger.error("Unauthorized delete attempt detected! Requester IP: %s, Server IP (hardcoded): %s " % (request_ip, server_ip))
else:
    form = cgi.FieldStorage()
    # Get data from fields
    if form.getvalue('job'):
        job = form.getvalue('job')
        mainLogger.debug("Apache cleaner received job id: %s. (Request IP: %s, Server IP: %s)" % (job, request_ip, server_ip))
        try:
            shutil.rmtree(jobsdir+'/'+job)
            mainLogger.debug("Deleted job: %s." % job)
            body["status"] = "Success"
            body["job"] = job
        except Exception, e:
            mainLogger.error("Received exception: %s" % str(e))
            body["job"] = job

print "Status: 200 OK"
print "Content-Type: application/json"
print "Length:", len(body)
print ""
print json.JSONEncoder().encode(body)
