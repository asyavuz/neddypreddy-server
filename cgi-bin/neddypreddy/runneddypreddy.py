#!/disk2/shared/soft/bin/python
__author__ = 'asyavuz'
import os
import sys
import math
import json
import time
import tools
import shutil
import datetime
import logging
import sequence
import mysql.connector
from multiprocessing import Pool

# Load config variables
try:
    config = json.load(open('../config.json'))
except Exception, e:
    sys.stderr.write(str(e)+"\n")
    sys.exit(1)

# Set environment variables
for var in config['environmentVars']:
    os.environ[var['name']] = var['value']

CONCURRENT_JOBS = config['server']['concurrentJobs']
MAX_CORE_PER_JOB = config['server']['MAX_CORE_PER_JOB']
jobsdir = config['server']['jobsDir']

def setup_logger(logger_name, log_file, level=logging.INFO):
    l = logging.getLogger(logger_name)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fileHandler = logging.FileHandler(log_file, mode='w')
    fileHandler.setFormatter(formatter)
    streamHandler = logging.StreamHandler()
    streamHandler.setFormatter(formatter)

    l.setLevel(level)
    l.addHandler(fileHandler)
    l.addHandler(streamHandler)


# Define runner function
def neddypreddy(jobid):
    # Start with creating a job-specific logger
    setup_logger('job'+jobid, jobsdir+'/'+jobid+'/main.log', logging.DEBUG)
    logger = logging.getLogger('job'+jobid)
    logger.info("Trying to connect mySQL server...")
    try:
        cnx = mysql.connector.connect(user=config['mysql']['username'], password=config['mysql']['password'],
                                      host=config['mysql']['host'],
                                      database=config['mysql']['database'])
    except Exception, e:
        tools.send_email("", 'neddypreddy@gmail.com', 'Incident: mySQL Related Problem!',
                         '<br /> or <br /><br />Dear Admin,<br />Your server configuration is not working properly at the '
                         'moment. We have received following error: %s. <br /><br />Please fix this problem as soon as '
                         'possible.<br /><br />Sincerely,<br />NeddyPreddy Server' % str(e))
        raise e
    # Get sequences
    logger.info("Getting sequences...")
    sequences = tools.get_sequences(jobid, jobsdir+jobid+'/'+jobid+'.fasta')

    cursor = cnx.cursor()
    cursor.execute("SELECT email FROM queue WHERE jobid='%s';" % jobid)
    for mail in cursor:
        email = mail[0]
    cursor.close()
    # Update job as running
    cursor = cnx.cursor()
    cursor.execute("UPDATE queue SET status=%s WHERE jobid='%s' " % (str(1), jobid))
    cnx.commit()
    cursor.close()

    completed = 0
    complete_seqs = []
    uniprot_ids = {}
    for i, seq in enumerate(sequences):
        tmp_identifier = tools.id_generator()
        logger.info("Starting to work on protein %d of %d (ID: %s, Real: %s)..." % (i+1, len(sequences), tmp_identifier,
                                                                                    seq.id))

        uniprot = tools.obtain_uniprot(tmp_identifier, seq, outdir=jobsdir+'/'+jobid, loggerid='job'+jobid)
        if not uniprot:
            logger.info("No suitable Uniprot ID was found in dbPTM protein set.")
        uniprot_ids[seq.id] = uniprot
        tools.update_status(jobid, "Waiting for IUPred result...", cnx)
                
        iupred = tools.predict_disorder(tmp_identifier, seq, outdir=jobsdir+'/'+jobid, loggerid='job'+jobid)
        if not iupred:
            tools.set_error(jobid, "NeddyPreddy has encountered an error during IUPred prediction!<br />Please try "
                                   "again later.<br />This error has been reported to the NeddyPreddy team.", cnx, email)
            sys.stderr.write("Problem in retrieving disorder predictions from IUPRED for %s.\n" % seq.id)
            logger.error("Problem in retrieving disorder predictions from IUPred for %s." % seq.id)
            return False

        tools.update_status(jobid, "Waiting for PSIBLAST result...", cnx)
        
        pssm = tools.obtain_pssm(tmp_identifier, seq, outdir=jobsdir+'/'+jobid, loggerid='job'+jobid)
        if not pssm:
            tools.set_error(jobid, "NeddyPreddy has encountered an error during PSIBLAST run!<br />Please try again "
                                   "later.<br />This error has been reported to the NeddyPreddy team.", cnx, email)
            sys.stderr.write("Problem in creating a PSSM matrix for %s. Please check that you have a working "
                             "BLAST+ installation." % seq.id)
            logger.error("Problem in retrieving PSSM from PSIBLAST for %s." % seq.id)
            return False
        
        logger.info("Creating Seq object of %s..." % seq.id)
        try:
            seq_obj = sequence.Seq(identifier=seq.id, sequence=str(seq.seq))
            seq_obj.add_iupred(jobsdir+'/'+jobid+'/'+tmp_identifier+'.iupred')
            seq_obj.add_pssm(jobsdir+'/'+jobid+'/'+tmp_identifier+'.pssm')
            seq_obj.set_aa_freq()

        except IOError as e:
            tools.set_error(jobid, "NeddyPreddy has encountered an error working on sequence predictions.<br />Please "
                                   "try again later.<br />This error has been reported to NeddyPreddy team.", cnx, email)
            logger.error("Problem in sequence object creation. Message: %s" % str(e))
            return False
            
        # We mark a sequence complete, as actual prediction part does not take that much time.
        complete_seqs.append(seq_obj)
        completed += 1
        tools.update_completed(jobid, completed, cnx)

    tools.update_status(jobid, "Predicting neddylation sites...", cnx)
    logger.info("Predicting neddylation sites...")

    # Load SVM model
    (clf, scaler, metadata) = tools.get_classifier(config['server']['dataPickle'], jobid)

    if not clf:
        tools.set_error(jobid, "NeddyPreddy has encountered an error working on sequence predictions.<br />Please "
                                   "try again later.<br />This error has been reported to NeddyPreddy team.", cnx, email)
        logger.error("An issue occurred during classifier training.\n")
        return False

    def get_window(seq, resloc, window_size):
        resloc -= 1  # Index of central lysine in complete sequence
        left = ""
        if resloc-((window_size-1)/2) < 0:
            missing = int(math.fabs(resloc-((window_size-1)/2)))
            left += '-'*missing
            left += seq.sequence[0:resloc]
        else:
            left = seq.sequence[(resloc-((window_size-1)/2)):resloc]

        right = ""
        if resloc+1+((window_size-1)/2) > len(seq.sequence):
            missing = (resloc+((window_size-1)/2)+1) - len(seq.sequence)
            right = seq.sequence[resloc+1:]
            right += '-'*missing
        else:
            right = seq.sequence[resloc+1:(resloc+1+((window_size-1)/2))]
        
        return left, right

    # Start determining residues to predict and predict!
    predictions = {'total_count': 0, 'default_predicted': 0, 'predicted_sites': {}, 'all_sites': {}}
    for seq in complete_seqs:
        logger.info("Predicting neddylation of status of lysines in %s..." % seq.identifier)
        predictions['predicted_sites'][seq.identifier] = []
        predictions['all_sites'][seq.identifier] = []

        for resid in seq:
            if not resid:
                continue
            if resid.aa == 'K':
                predictions['total_count'] += 1
                resid.set_selected()
                (pre_window, post_window) = get_window(seq, resid.no, metadata['WS'])
                disorder = resid.pro_features['Window-Disorder-Real']
                clf_ready = resid.get_instance(metadata['feats'], scaler).data

                decfunout = clf.decision_function(clf_ready).tolist()
                #if decfunout[0] is list: # scikit-learn returns decision value in a nested list in 0.15, in a list in 0.16
                #    dec_val = decfunout[0][0]
                #else:
                #    dec_val = decfunout[0]
                dec_val = decfunout[0]
                pred = clf.predict(clf_ready)
                prob = clf.predict_proba(clf_ready)[0][1]

                prediction = {
                    'identifier': seq.identifier,
                    'residue_no': resid.no,
                    'pre_window': pre_window,
                    'post_window': post_window,
                    'dec_val': dec_val,
                    'prob': prob,
                    'disorder': disorder,
                    'prot_len': len(seq.sequence),
                }

                if pred == 1:
                    predictions['default_predicted'] += 1
                    predictions['predicted_sites'][seq.identifier].append(prediction)

                predictions['all_sites'][seq.identifier].append(prediction)

    tools.update_status(jobid, "Preparing results page...", cnx)
    tools.prep_results(jobid, predictions, uniprot_ids)
    logger.info("Emailing completion notice...")

    if email:
        joburl = "%s/jobs/%s/" % (config['server']['host'], jobid)
        tools.send_email(jobid, email, "Job Completed: %s" % jobid, "Your job with ID %s has been successfully completed. "
                                                                    "You can view and download results at "
                                                                    "<a href=\"%s\">%s</a>.<br /><br />Please note that "
                                                                    "we may not keep prediction results after %d days."
                                                                    "<br /><br />Sincerely,<br />NeddyPreddy "
                                                                    "Server<br />" % (jobid, joburl, joburl, config['server']['keepJobsForDay']))
    # Update job as completed
    cursor = cnx.cursor()
    cursor.execute("UPDATE queue SET status=%s, finishdate='%s' WHERE jobid='%s' " % (str(2), str(datetime.datetime.now()), jobid))
    cnx.commit()
    cursor.close()
    logger.info("Job completed.")


# Define loggers
logging.basicConfig(filename=config['server']['logDir']+'/runner.log',
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.DEBUG)
mainLogger = logging.getLogger("main")

if __name__ == '__main__':
    # Connect mySQL server to fetch and process jobs
    mainLogger.info("Trying to connect mySQL server...")
    try:
        cnx = mysql.connector.connect(user=config['mysql']['username'], password=config['mysql']['password'],
                                      host=config['mysql']['host'],
                                      database=config['mysql']['database'])
    except Exception, e:
        tools.send_email("", config['server']['adminEmail'], 'Incident: mySQL Related Problem! (Runner)',
                         '<br /> or <br /><br />Dear Admin,<br />Your server configuration is not working properly at the '
                         'moment. We have received following error: %s. <br /><br />Please fix this problem as soon as '
                         'possible.<br /><br />Sincerely,<br />NeddyPreddy Server' % str(e))
        raise e
    cursor = cnx.cursor()

    # Get first job in priority queue
    mainLogger.info("Fetching priority queue...")
    query = "SELECT jobid FROM queue WHERE queue='priority' AND status=0 ORDER BY submitdate ASC LIMIT %s;" % str(CONCURRENT_JOBS)
    cursor.execute(query)

    jobs = []
    for jobid in cursor:
        jobs.append(jobid[0])

    priojobs = len(jobs)
    mainLogger.info("%d job(s) from priority queue have been added to running queue." % priojobs)

    # Get normal queue
    mainLogger.info("Fetching normal queue...")

    if priojobs < CONCURRENT_JOBS:
        fetchCount = CONCURRENT_JOBS-len(jobs)
        query = "SELECT jobid FROM queue WHERE queue='normal' AND status=0 ORDER BY submitdate ASC LIMIT %s;" % str(fetchCount)
        cursor.execute(query)

        for jobid in cursor:
            jobs.append(jobid[0])

    normjobs = len(jobs) - priojobs
    mainLogger.info("%d job(s) from normal queue have been added to running queue." % normjobs)

    if jobs:
        mainLogger.info("Starting %d concurrent jobs..." % CONCURRENT_JOBS)
        #neddypreddy(jobs[0])
        pool = Pool(CONCURRENT_JOBS)
        results = pool.map(neddypreddy, jobs)
        pool.close()
        pool.join()
    mainLogger.info("This cycle completed. Sleeping until next cron job.")

    # Close down the database connection
    cursor.close()
    cnx.close()



