__author__ = 'asyavuz'
import os
import re
import sys
import json
import copy
import shlex
import shutil
import random
import string
import smtplib
import logging
import cPickle
import datetime
import tempfile
import subprocess
from Bio import SeqIO
import mysql.connector
from sklearn import svm
from sklearn import preprocessing
from collections import namedtuple
from email.mime.text import MIMEText
from operator import attrgetter, itemgetter
from email.mime.multipart import MIMEMultipart
try:
    config = json.load(open('../config.json'))
except Exception, e:
    sys.stderr.write(str(e)+"\n")
    sys.exit(1)

for var in config['environmentVars']:
    os.environ[var['name']] = var['value']

MAX_CORE_PER_JOB = config['server']['MAX_CORE_PER_JOB']

jobsdir = config['server']['jobsDir']

def write_prediction_row(prediction, other_predictions, thresholds, no, known_sites):
    row_html = ""
    row_html += "		  <tr>\n"
    row_html += "          <td>%s</td>\n" % prediction['identifier']
    row_html += "          <td>%d</td>\n" % prediction['residue_no']
    row_html += "          <td>%s<font color=#28BD67>K</font>%s</td>\n" % (prediction['pre_window'], prediction['post_window'])
    row_html += "          <td>%.2f</td>\n" % prediction['dec_val']
    row_html += "          <td>%.2f</td>\n" % prediction['prob']
    row_html += "          <td>\n"
    row_html += "            <p type=\"button\" class=\"btn btn-more\" data-toggle=\"modal\" data-target=\"#neddylationModal%d\">More</p>\n" % no
    row_html += "          <div class=\"col-md-1\"></div>\n"
    row_html += "          <div class=\"modal fade bs-example-modal-lg\" id=\"neddylationModal%d\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">\n" % no
    row_html += "            <div class=\"modal-dialog modal-lg\">\n"
    row_html += "              <div class=\"modal-content\">\n"
    row_html += "                <div class=\"modal-header\">\n"
    row_html += "                  <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>\n"
    row_html += "                  <h4 class=\"modal-title\" id=\"myModalLabel\">Neddylation Site Info</h4>\n"
    row_html += "                </div>\n"
    row_html += "                <div class=\"modal-body\">\n"
    row_html += "                  <div class=\"row\">\n"
    row_html += "                    <div class=\"col-md-12\">\n"
    row_html += "                    <table class=\"table\">\n"
    row_html += "                      <thead>\n"
    row_html += "                  			<tr>\n"
    row_html += "                          <th>ID</th>\n"
    row_html += "                          <th>Position</th>\n"
    row_html += "                          <th>Peptide</th>\n"
    row_html += "                          <th>Average Disorder</th>\n"
    row_html += "                          <th>Score</th>\n"
    row_html += "                          <th>Probability</th>\n"
    row_html += "                        </tr>\n"
    row_html += "                      </thead>\n"
    row_html += "                      <tbody>\n"
    row_html += "                        <tr>\n"
    row_html += "                        <td>%s</td>\n" % prediction['identifier']
    row_html += "                        <td>%d</td>\n" % prediction['residue_no']
    row_html += "                        <td>%s<font color=#28BD67>K</font>%s</td>\n" % (prediction['pre_window'], prediction['post_window'])
    row_html += "                        <td>%.2f</td>\n" % prediction['disorder']
    row_html += "                        <td>%.2f</td>\n" % prediction['dec_val']
    row_html += "                        <td>%.2f</td>\n" % prediction['prob']
    row_html += "                        </tr>\n"
    row_html += "                      </tbody>\n"
    row_html += "                    </table>\n"
    row_html += "                    </div>\n"
    row_html += "                    <div class=\"col-md-12\"><div class=\"separator\"></div></div>\n"
    row_html += "                    <div class=\"col-md-12\">\n"
    row_html += "                      <br>\n"
    # Start working on visualization
    mod_sum = {}

    # Determine xper_residue for different regions of visualization, expand left and rightmost 10% visualization
    xper_residue = float(755/float(prediction['prot_len']))

    for site in known_sites:
        xcoor = 5+xper_residue*site['aa']

        mod = ""
        if site['modification'] == "Acetylation":
            mod = "Ace"
        elif site['modification'] == "Sumoylation":
            mod = "Sum"
        elif site['modification'] == "Ubiquitylation":
            mod = "Ubi"
        elif site['modification'] == "O-linked Glycosylation":
            mod = "Gly"
        elif site['modification'] == "Methylation":
            mod = "Met"

        if site['aa'] in mod_sum.keys():
            mod_sum[site['aa']]['mods'].append(mod)
        else:
            mod_sum[site['aa']] = {'xcoor': xcoor, 'mods': [mod]}

    # Check maximum amount of modifications in a single location
    max_site = 0
    for loc in mod_sum.keys():
        if len(mod_sum[loc]['mods']) > max_site:
            max_site = len(mod_sum[loc]['mods'])
            
    offset = (max_site+1)*9 # Allows extra 2 modifications for superscripting
    row_html += "                      <h5>Position on the Protein with Other Predicted and Known Sites</h5>\n"
    row_html += "                      <svg width=\"775\" height=\"%d\">\n" % (70+offset)
    row_html += "                        <rect x=\"5\" y=\"%d\" width=\"755\" height=\"16\" style=\"fill:#E5E5E5;stroke-width:2\" />\n" % (10+offset)
    row_html += "                        <line x1=\"5\" y1=\"%d\" x2=\"5\" y2=\"%d\" style=\"stroke:#A9A9A9;stroke-width:2\" />\n" % (10+offset, 26+offset)
    row_html += "                        <text x=\"5\" y=\"%d\" font-size=\"8px\" fill=\"#A9A9A9\" text-anchor=\"start\">1</text>\n" % (7+offset)

    # Pool all predictions for bar graph in the next section
    all_preds = []
    pred_thr = []
    pred_locs = []
    down_labels = []
    # Draw other predictions and write threshold legend
    no_of_thr = len(thresholds)

    xcoor_start = int(round((755-(no_of_thr*100))/float(2)))-5
    for i, thr in enumerate(reversed(thresholds)):
        xcoor = 5+xcoor_start+(i*100)
        thr_color = config['server']['thresholdColors'][thr.name]
        row_html += "                       <rect x=\"%d\" y=\"%d\" width=\"10\" height=\"10\" style=\"fill:%s;stroke-width:2\" />\n" % (xcoor, 55+offset, thr_color)
        row_html += "					    <text x=\"%d\" y=\"%d\" font-size=\"13px\" fill=\"%s\" text-anchor=\"begin\" alignment-baseline=\"baseline\">%s</text>\n" % (xcoor+20, 55+offset, thr_color, thr.name)

        for pred in other_predictions[thr.name]:
            if pred['residue_no'] == prediction['residue_no'] or pred['identifier'] != prediction['identifier']:
                continue
            all_preds.append(pred)
            pred_thr.append(thr.name)
            pred_locs.append(pred['residue_no'])
            xcoor = 5+xper_residue*pred['residue_no']
            row_html += "                        <line x1=\"%f\" y1=\"%d\" x2=\"%f\" y2=\"%d\" style=\"stroke:%s;stroke-width:2\" />\n" % (xcoor, 10+offset, xcoor, 26+offset, thr_color)

    # Actual prediction is drawn here.
    xcoor = 5+xper_residue*prediction['residue_no']
    row_html += "                        <line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\" style=\"stroke:#6325F4;stroke-width:2\" />\n" % (xcoor, 10+offset, xcoor, 28+offset)
    up_position_act = "                        <text x=\"%d\" y=\"%d\" font-size=\"13px\" fill=\"#6325F4\" text-anchor=\"middle\">%d</text>\n" % (xcoor, 38+offset, prediction['residue_no'])
    down_position_act = "                        <text x=\"%d\" y=\"%d\" font-size=\"13px\" fill=\"#6325F4\" text-anchor=\"middle\">%d</text>\n" % (xcoor, 52+offset, prediction['residue_no'])
    pred_locs.append(prediction['residue_no']) # Add actual prediction to prediction locations before formatting                                                                                                                

    all_preds.append(prediction)
    pred_thr.append("Original")
    
    all_preds_sorted = [y for (y,x) in sorted(zip(all_preds, pred_thr), key=lambda pair: pair[0]['residue_no'])]
    pred_thr = [x for (y,x) in sorted(zip(all_preds, pred_thr), key=lambda pair: pair[0]['residue_no'])]

    for i, pred in enumerate(all_preds_sorted):
        res = pred['residue_no']

        thr_name = pred_thr[i]
        if res != prediction['residue_no']:
            thr_color = config['server']['thresholdColors'][thr_name]
        else:
            thr_color = "#000000"

        xcoor = 5+xper_residue*pred['residue_no']
        up_position = "                        <text x=\"%f\" y=\"%d\" font-size=\"10px\" fill=\"%s\" text-anchor=\"middle\">%d</text>\n" % (xcoor, 38+offset, thr_color, pred['residue_no'])
        down_position = "                        <text x=\"%f\" y=\"%d\" font-size=\"10px\" fill=\"%s\" text-anchor=\"middle\">%d</text>\n" % (xcoor, 48+offset, thr_color, pred['residue_no'])
        
        if res-1 in pred_locs and res-1 not in down_labels:
            if res == prediction['residue_no']:
                row_html += down_position_act
            else:
                row_html += down_position
            down_labels.append(res)
        elif res-2 in pred_locs and res-2 not in down_labels:
            if res == prediction['residue_no']:
                row_html += down_position_act
            else:
                row_html += down_position
            down_labels.append(res)
        else:
            if res == prediction['residue_no']:
                row_html += up_position_act
            else:
                row_html += up_position
        
    row_html += "                                                \n"
    up_labels = [] # Up labels for modifications
    # Draw known sites
    for loc in sorted(mod_sum.keys()):
        if loc not in pred_locs:
            row_html += "                        <line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\" style=\"stroke:#A9A9A9;stroke-width:2\" />\n" % (mod_sum[loc]['xcoor'], 10+offset, mod_sum[loc]['xcoor'], 26+offset)
            row_html += "                        <text x=\"%d\" y=\"%d\" font-size=\"8px\" fill=\"#A9A9A9\" text-anchor=\"middle\">%d</text>\n" % (mod_sum[loc]['xcoor'], 38+offset, loc)
        if loc-1 in mod_sum.keys() and loc-1 not in up_labels:
            starty = offset
            up_labels.append(loc)
        elif loc-2 in mod_sum.keys() and loc-2 not in up_labels:
            starty = offset
            up_labels.append(loc)
        else:
            starty = 7+offset
        for m, mod in enumerate(mod_sum[loc]['mods']):
            y = starty - 7*m
            row_html += "                        <text x=\"%d\" y=\"%d\" font-size=\"8px\" fill=\"#A9A9A9\" text-anchor=\"middle\">%s</text>\n" % (mod_sum[loc]['xcoor'], y, mod)

    if prediction['prot_len'] not in pred_locs:
        row_html += "						 <line x1=\"760\" y1=\"%d\" x2=\"760\" y2=\"%d\" style=\"stroke:#A9A9A9;stroke-width:2\" />\n" % (10+offset, 26+offset)
    row_html += "						 <text x=\"760\" y=\"%d\" font-size=\"8px\" fill=\"#A9A9A9\" text-anchor=\"end\">%d</text>\n" % (7+offset, prediction['prot_len'])
    row_html += "						 \n"

    row_html += "                      </svg>\n"
    row_html += "                    </div>\n"
    row_html += "                    <div class=\"col-md-12\">  <div class=\"separator\"></div></div>\n"
    row_html += "                    <div class=\"col-md-12\">\n"
    row_html += "                      <h5>Compare with Other Sites on the Protein</h5>\n"
    row_html += "                      <div id=\"chart%d\" style=\"height: 200px;\"></div>\n" % no
    row_html += "                      <script>\n"
    row_html += "                      $( \"#neddylationModal%d\" ).on('shown.bs.modal', function() {\n" % no
    row_html += "                        var chart = c3.generate({\n"
    row_html += "                        bindto: \"#chart%d\",\n" % no
    row_html += "                            data: {\n"
    row_html += "                                x : 'x',\n"
    row_html += "                                columns: [\n"

    # Now let's generate bar graph
    dp_x = "['x', "
    dp_s = "['score', "
    dp_p = "['probability', "
    all_preds.append(prediction)
    all_preds = sorted(all_preds, key=itemgetter('residue_no'))
    for pred in all_preds:
        if prediction['identifier'] != pred['identifier']:
            continue
        dp_x += "'%d', " % pred['residue_no']
        dp_s += "'%.2f', " % pred['dec_val']
        dp_p += "'%.2f', " % pred['prob']
    row_html += "                                    %s],\n" % dp_x[:-2]
    row_html += "                                    %s],\n" % dp_s[:-2]
    row_html += "                                    %s],\n" % dp_p[:-2]
    row_html += "                                ],\n"
    row_html += "                                type: 'bar',\n"
    row_html += "                                colors: {\n"
    row_html += "                                      x: '#ff0000',\n"
    row_html += "                                      score: '#6334F0',\n"
    row_html += "                                      probability: '#398AC5'\n"
    row_html += "                                  },\n"
    row_html += "                            },\n"
    row_html += "                            axis: {\n"
    row_html += "                                x: {\n"
    row_html += "                                    type: 'category',\n"
    row_html += "                                },\n"
    row_html += "                                y : {\n"
    row_html += "                                    tick: {\n"
    row_html += "                                              format: d3.format(\".2r\")\n"
    row_html += "                                    },\n"
    row_html += "                                },\n"
    row_html += "                            },\n"
    row_html += "                        });\n"
    row_html += "                      });\n"
    row_html += "                      </script>\n"
    row_html += "                    </div>\n"
    row_html += "                  </div>\n"

    #EDITED HERE BELOW - 04.01.2016
    row_html += "                    <div class=\"col-md-12\"><div class=\"separator\"></div></div>\n"
    row_html += "                    <div class=\"col-md-12\">\n"
    row_html += "                      <br>\n"
    row_html += "                      <h5>Other Lysine Modifications Located on Protein (Source: <a href=\"http://dbptm.mbc.nctu.edu.tw\" target=\"_blank\">dbPTM</a>)</h5>\n"
    row_html += "                    <table class=\"table\">\n"
    row_html += "                      <thead>\n"
    row_html += "                        <tr>\n"
    row_html += "                          <th>Sequence ID</th>\n"
    row_html += "                          <th>Uniprot ID</th>\n"
    row_html += "                          <th>Position</th>\n"
    row_html += "                          <th>Modification</th>\n"
    row_html += "                          <th>Reference</th>\n"
    row_html += "                          <th>Source</th>\n"
    row_html += "                        </tr>\n"
    row_html += "                      </thead>\n"
    row_html += "                      <tbody>\n"
    # Prepare a db connection for source linking
    try:
        cnx = mysql.connector.connect(user=config['mysql']['username'], password=config['mysql']['password'],
                                      host=config['mysql']['host'],
                                      database=config['mysql']['database'])
    except Exception, e:
        send_email("", 'neddypreddy@gmail.com', 'Incident: mySQL Related Problem!',
                         '<br /> or <br /><br />Dear Admin,<br />Your server configuration is not working properly at the '
                         'moment. We have received following error: %s. <br /><br />Please fix this problem as soon as '
                         'possible.<br /><br />Sincerely,<br />NeddyPreddy Server' % str(e))
        raise e

    for site in known_sites:
        row_html += "                        <tr>\n"
        row_html += "                        <td>%s</td>\n" % site['orig_id']
        row_html += "                        <td>%s</td>\n" % site['uniprot_id']
        row_html += "                        <td>%d</td>\n" % site['aa']
        row_html += "                        <td>%s</td>\n" % site['modification']
        references = site['pubmed'].split(";")
        ref_txt = ""
        for reference in references:
            ref_txt += '<a href="http://www.ncbi.nlm.nih.gov/pubmed/%s" target="_blank">%s</a><br />' % (reference, reference)
        if ref_txt.endswith("<br />"):
            ref_txt = ref_txt[:-6]
        row_html += "                        <td>%s</td>\n" % ref_txt

        # Get xref IDs for source linking
        xref_dict = None
        cursor = cnx.cursor()
        cursor.execute("SELECT * FROM xref WHERE uniprot='%s';" % site['uniprot_id'])
        for record in cursor:
            xref_dict = {'uniprot': record[0], 'gene': record[1], 'ncbi': record[2], 'sysptm': record[3], 'hprd': record[4]}
        cursor.close()
        if not xref_dict:
            xref_dict = {'uniprot': '', 'gene': '', 'ncbi': '', 'sysptm': '', 'hprd': ''}
    
        sources = site['source'].split(";")
        src_txt = ""
        for source in sources:
            src_link = ""
            if source.startswith("HPRD"):
                src_link = "http://www.hprd.org/protein/%s" % xref_dict['hprd']
            elif source.startswith("Swiss-Prot"):
                src_link = "http://www.uniprot.org/uniprot/%s" % site['uniprot_id']
            elif source.startswith("Phosphositeplus"):
                src_link = "http://www.phosphosite.org/uniprotAccAction.do?id=%s" % site['uniprot_id']
            elif source.startswith("SysPTM"):
                src_link = "http://www.biosino.org/SysPTM/protein.jsp?proteinID=%s" % xref_dict['sysptm']
            elif source.startswith("UbiProtDB"):
                src_link = "http://ubiprot.org.ru/index.php?mode=proteins_show&spac=%s" % site['uniprot_id']

            src_txt += '<a href="%s" target="_blank">%s</a><br />' % (src_link, source)
        if src_txt.endswith("<br />"):
                src_txt = src_txt[:-6]

        row_html += "                        <td>%s</td>\n" % src_txt
        row_html += "                        </tr>\n"
    row_html += "                      </tbody>\n"
    row_html += "                    </table>\n"
    row_html += "                    </div>\n"
    row_html += "                    <div class=\"col-md-12\"><div class=\"separator\"></div></div>\n"
    # EDIT ENDS
    row_html += "                <div class=\"modal-footer\">\n"
    row_html += "                  <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Close</button>\n"
    row_html += "                </div>\n"
    row_html += "              </div>\n"
    row_html += "            </div>\n"
    row_html += "           </div>\n"
    row_html += "          </div>\n"
    row_html += "          </td>\n"
    row_html += "          </tr>\n"
    cnx.close()
    return row_html

def send_email(jobid, email, subject, message, cc=""):
    '''
    E-mail sending helper function
    :param jobid: ID of job
    :param email: Recipient e-mail address
    :param subject: E-mail subject
    :param message: Message of email
    :param cc: Carbon-copy recipient (if needed)
    :return: Nothing
    '''
    logger = logging.getLogger('job'+jobid)

    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject
    msg['From'] = config['email']['from']
    msg['To'] = email
    if cc:
        msg['Cc'] = cc
        email = email+', '+cc

    plainmsg = 'Dear User,\n'+message+' \n\n'
    plainmsg = plainmsg.replace('<br />', '\n')
    plainmsg = re.sub(r'<a href=".*?">(.*?)</a>', r'\1', plainmsg)

    htmlmessage = """\
    <html>
      <head></head>
      <body>
        <p>Dear User,<br />
            %s
        </p>
      </body>
    </html>
    """ % message

    # Record the MIME types of both parts - text/plain and text/html.
    part1 = MIMEText(plainmsg, 'plain')
    part2 = MIMEText(htmlmessage, 'html')

    # Attach parts into message container.
    # According to RFC 2046, the last part of a multipart message, in this case
    # the HTML message, is best and preferred.
    msg.attach(part1)
    msg.attach(part2)
    if config['email']['provider'] == 'smtp':
        logger.info("Sending email with subject: %s to: %s..." % (subject, email))
        user = config['email']['smtp']['username']
        pwd = config['email']['smtp']['password']

        try:
            smtpserver = smtplib.SMTP(config['email']['smtp']['host'], config['email']['smtp']['port'])
            smtpserver.ehlo()
            smtpserver.starttls()
            smtpserver.ehlo()
            smtpserver.login(user, pwd)
            smtpserver.sendmail(user, email, msg.as_string())
            smtpserver.close()
        except BaseException as e1:
            logger.error("Problem in sending email with SMTP. Exception message: %s." % str(e1))
            if config['email']['smtp']['fallback'] == 'sendmail':
                try:
                    sendmail = config['email']['sendmail']['executable']
                    header = 'To:' + email + '\n' + 'From: ' + config['email']['from'] + '\n' + 'Subject:'+subject+' \n'

                    p = os.popen("%s -t -i" % sendmail, "w")
                    p.write(header+'\n'+plainmsg)
                    status = p.close()
                    if status:
                        raise Exception("Sendmail exit status"+str(status))
                except BaseException as e:
                    logger.error("Problem in sending email with sendmail. Exception message: %s." % str(e))
                    raise(e)

    elif config['email']['provider'] == 'sendmail':
        try:
            sendmail = config['email']['sendmail']['executable']
            header = 'To:' + email + '\n' + 'From: ' + config['email']['from'] + '\n' + 'Subject:'+subject+' \n'

            p = os.popen("%s -t -i" % sendmail, "w")
            p.write(header+'\n'+plainmsg)
            status = p.close()
            if status:
                raise Exception("Sendmail exit status"+str(status))
        except BaseException as e:
            logger.error("Problem in sending email with sendmail. Exception message: %s." % str(e))
            raise(e)

def set_error(jobid, message, cnx, email=""):
    '''
    Sets error message in the job folder. Also, marks job as failed in the queue.
    :param jobid: ID of job
    :param message: Error message
    :param cnx: Database connection object
    :param email: E-mail notification recipient address
    :return: Nothing
    '''
    logger = logging.getLogger('job'+jobid)
    logger.info("Preparing error page...")
    try:
        ifh = open('../templates/error.html')
        error_template = ifh.read()
        ifh.close()
        ofh = open(jobsdir+jobid+'/index.html', 'w')
        ofh.write(error_template.replace("<!--ERRORTEXT-->", message))
        ofh.close()
        ofh = open(jobsdir+jobid+'/ERROR.txt', 'w')
        ofh.close()
        cursor = cnx.cursor()
        cursor.execute("UPDATE queue SET status=%s WHERE jobid='%s' " % (str(-1), jobid))
        cnx.commit()
        cursor.close()
    except Exception as e:
        logger.error("Problem in updating the status of in progress page. Exception message: %s." % str(e))
        raise(e)
    if email:
        email_msg = message
        email_msg += " Reasons of this error will be thoroughly investigated.<br /><br />We apologize for any inconvenience caused.<br /><br />"
        email_msg += "Sincerely,<br />NeddyPreddy Team"
        send_email(jobid, email, 'Error in Job ID: %s' %jobid, email_msg, cc=config['server']['adminEmail'])
    else:
        email_msg = message
        email_msg += "<br />Please investigate this error thoroughly.<br /><br />"
        email_msg += "Sincerely,<br />NeddyPreddy Server"
        send_email(jobid, config['server']['adminEmail'], 'Incident: Error in Job ID: %s' % jobid, email_msg)


def update_status(jobid, message, cnx):
    '''
    Updates status comment field on the database
    :param jobid: ID of job
    :param message: Updated message
    :param cnx: Database connection
    :return: Nothing
    '''
    logger = logging.getLogger('job'+jobid)
    try:
        cursor = cnx.cursor()
        cursor.execute("UPDATE queue SET statuscomment='%s' WHERE jobid='%s' " % (message, jobid))
        cnx.commit()
        cursor.close()
    except BaseException as e:
        logger.info("Problem in updating the status of in progress page. Exception message: %s." % str(e))
        raise(e)

def update_completed(jobid, completed, cnx):
    '''
    Update the amount of proteins completed on the database
    :param jobid: ID of job
    :param completed: Amount of proteins completed
    :param cnx: Database connection
    :return: Nothing
    '''
    logger = logging.getLogger('job'+jobid)

    try:
        cursor = cnx.cursor()
        cursor.execute("UPDATE queue SET completed=%s WHERE jobid='%s' " % (str(completed), jobid))
        cnx.commit()
        cursor.close()
    except BaseException as e:
        logger.info("Problem in updating the completed protein number of in progress page. Exception message: %s." % str(e))
        raise(e)

def prep_results(jobid, predictions, uniprot_ids):
    '''
    Prepares results page
    :param jobid: ID of job
    :param predictions: List of predictions
    :param uniprot: identified uniprot id
    :return:
    '''
    finish_date = datetime.datetime.now()
    exp_date_obj = finish_date + datetime.timedelta(days=config['server']['keepJobsForDay'])
    expiry_date = exp_date_obj.strftime("%m/%d/%Y %I:%M %p")
    expiry_date_friendly = exp_date_obj.strftime("%d %B %Y %H:%M")
    logger = logging.getLogger('job'+jobid)
    ifh = open('../templates/index.html')
    index_template = ifh.read()
    ifh.close()

    ifh = open('../templates/details.html')
    details_template = ifh.read()
    ifh.close()

    ifh = open('../templates/start_over.html')
    startover_template = ifh.read()
    ifh.close()

    # Set citation info of these pages
    index_template = index_template.replace("<!--CITATION_INFO-->", str(config['server']['citation']))
    details_template = details_template.replace("<!--CITATION_INFO-->", str(config['server']['citation']))
    startover_template = startover_template.replace("<!--CITATION_INFO-->", str(config['server']['citation']))

    logger.info("Formatting the results index page...")

    index_template = index_template.replace('<!--END_DATE-->', expiry_date)
    index_template = index_template.replace('<!--EXPIRY_DAYS-->', str(config['server']['keepJobsForDay']))

    startover_template = startover_template.replace('<!--END_DATE-->', expiry_date_friendly)

    index_template = index_template.replace('<!--TOTAL_SITES-->', str(predictions['total_count']))
    index_template = index_template.replace('<!--NEDDYLATION_SITES-->', str(predictions['default_predicted']))
    index_template = index_template.replace('<!--NON_NEDDYLATION_SITES-->', str((predictions['total_count']-predictions['default_predicted'])))

    dp_x = "['x', "
    dp_s = "['sites', "
    for identifier in predictions['predicted_sites'].keys():
        dp_x += '\''+identifier+'\', '
        dp_s += str(len(predictions['predicted_sites'][identifier]))+', '

    xvals = dp_x[:-2]
    svals = dp_s[:-2]
    xvals += '],\n'
    svals += '],'

    index_txt = index_template.replace('<!--COLUMNS-->', str(xvals+svals))
    logger.info("Writing index page...")
    try:
        ofh = open(jobsdir+jobid+'/index.html', 'w')
        ofh.write(index_txt)
        ofh.close()
        shutil.copy('../templates/csv.php', jobsdir+jobid+'/')
    except Exception, e:
        logger.error("Problem in writing out the results page. Exception message: %s." % str(e))
        raise e

    # Start working on details page
    logger.info("Formatting details page...")
    Threshold = namedtuple("Threshold", ["name", "value"])
    thresholds_html = ""
    thresholds = []

    # Lets get predefined thresholds from config gile
    for thr in config['server']['thresholds'].keys():
        val = config['server']['thresholds'][thr]

        t = Threshold(name=thr, value=val)
        thresholds.append(t)

    # Sort them so the highest threshold comes first
    thresholds = sorted(thresholds, key=attrgetter('value'), reverse=True)

    # Then use reversed list for nicer look (weird)
    for thr in reversed(thresholds):
        thresholds_html += "               <li><button type=\"button\" class=\"btn btn-treshold\" " \
                           "data-filter-column=\"3\" data-filter-text=\">="+str(thr.value)+"\">"+thr.name+"</button></li>\n"

    # Write out nice threshold options
    details_template = details_template.replace("<!--THRESHOLDS-->", str(thresholds_html))

    details_entries = ""
    entry_no = 1
    for identifier in predictions['all_sites'].keys():
        pred_dif_thr = {}
        for thr in thresholds:
            pred_dif_thr[thr.name] = []

        # First populate threshold divided dictionary for better visualization
        for prediction in predictions['all_sites'][identifier]:
            added = False
            for thr in thresholds:
                if prediction['dec_val'] >= thr.value and not added:
                    pred_dif_thr[thr.name].append(prediction)
                    added = True

        # Check for experimentally identified other modifications
        known_sites = []
        uniprot = uniprot_ids[identifier]
        if uniprot:
            try:
                cnx = mysql.connector.connect(user=config['mysql']['username'], password=config['mysql']['password'],
                                              host=config['mysql']['host'],
                                              database=config['mysql']['database'])
            except Exception, e:
                send_email("", 'neddypreddy@gmail.com', 'Incident: mySQL Related Problem!',
                                 '<br /> or <br /><br />Dear Admin,<br />Your server configuration is not working properly at the '
                                 'moment. We have received following error: %s. <br /><br />Please fix this problem as soon as '
                                 'possible.<br /><br />Sincerely,<br />NeddyPreddy Server' % str(e))
                raise e
            
            cursor = cnx.cursor()
            cursor.execute("SELECT * FROM dbPTM WHERE uniprot='%s' ORDER BY aa ASC, modification ASC;" % uniprot)
            for record in cursor:
                known_sites.append({'orig_id': identifier, 'uniprot_id': record[2], 'aa': record[3], 'pubmed': record[5], 'source': record[6], 'modification': record[8]})
            cursor.close()
            cnx.close()

        # Now generate HTML of all prediction results
        for prediction in predictions['all_sites'][identifier]:
            entry_no += 1
            details_entries += write_prediction_row(prediction, pred_dif_thr, thresholds, entry_no, known_sites)
            
    details_txt = details_template.replace("<!--PREDICTIONS-->", str(details_entries))
    logger.info("Writing details page...")
    try:
        ofh = open(jobsdir+jobid+'/details.html', 'w')
        ofh.write(details_txt)
        ofh.close()
    except Exception, e:
        logger.error("Problem in writing out the details page. Exception message: %s." % str(e))
        raise e

    # Write out start over page
    logger.info("Writing start over page...")
    try:
        ofh = open(jobsdir+jobid+'/start_over.html', 'w')
        ofh.write(startover_template)
        ofh.close()
    except Exception, e:
        logger.error("Problem in writing out the details page. Exception message: %s." % str(e))
        raise e

    # Now we are done! Phew! Woo hoo!

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    '''
    Generates temporary ID
    :param size: Size of the ID
    :param chars: Allowed characters
    :return:
    '''
    return ''.join(random.choice(chars) for _ in range(size))

def get_sequences(jobid, file):
    '''
    Parses FASTA formatted file and returns list of sequences
    :param jobid: ID of job
    :param file: Filename
    :return:
    '''
    #Load sequences
    logger = logging.getLogger('job'+jobid)
    try:
        ifh = open(file)
    except IOError:
        sys.stderr.write("File not found or cannot be accessible: %s.\n" % file)
        logger.error("File not found or cannot be accessible: %s." % file)
        sys.exit(1)

    sequences = list(SeqIO.parse(ifh, 'fasta'))

    ifh.close()
    return sequences

def obtain_uniprot(tmp_identifier, sequence, outdir, loggerid):
    '''
    Performs BLAST search on dbPTM protein set to identify possible Uniprot ID.

    :param tmp_identifier: Temporary identifier of the sequence
    :param sequence: Sequence object
    :param outdir: Output directory
    :param loggerid: ID of logger
    :return: Returns found Uniprot ID or None if fails
    '''

    blast_exec = "blastp"
    logger = logging.getLogger(loggerid)

    if not os.path.exists(outdir+'/'+tmp_identifier+'.fasta'):
                tmp = open(outdir+'/'+tmp_identifier+'.fasta', 'w')
                tmp.write(sequence.format('fasta'))
                tmp.close()
    logger.info("Running BLAST search on dbPTM-prot for %s..." % sequence.id)

    command = blast_exec+' -query '+outdir+'/'+tmp_identifier+'.fasta -num_threads '+str(MAX_CORE_PER_JOB)
    command += ' -db dbPTM-prot -outfmt "6 qseqid qlen sseqid slen pident nident gaps" -num_alignments 1'
    logger.debug("Running command: %s" % command)
    args = shlex.split(command)

    blast_output = tempfile.NamedTemporaryFile(prefix='bl', suffix='.out', delete=False)
    logger.debug("Writing BLAST output to %s" % blast_output.name)
    try:
        process = subprocess.Popen(args, stdout=blast_output, stderr=subprocess.PIPE)
    except OSError:
        logger.error("Cannot find blastp installation. Please make sure you installed BLAST+ and set PATH variable"
                     " correctly.")
        sys.stderr.write("Cannot find psiblast installation. Please make sure you installed BLAST+ and set "
                         "PATH variable correctly.\n")
        blast_output.close()
        return False
    
    for line in process.stderr:
        logger.error(line)
        
    process.wait()
        
    logger.info("BLASTp run of "+outdir+"/"+tmp_identifier+".fasta finished with code "+str(process.returncode)+".")
    if int(process.returncode) == 0:
        blast_output.file.seek(0)
        blast_results = blast_output.file.readlines()
        uniprot = None
        for i, line in enumerate(blast_results):
            if not uniprot:
                elems = line.split("\t")
                qlen = elems[1]
                slen = elems[3]
                pident = float(elems[4])
                sseqid = elems[2]
                if slen == qlen and pident == 100:
                    id_elems = sseqid.split("|")
                    uniprot = id_elems[1]
            else:
                break
        blast_output.close()
        logger.info("For %s, BLASTp identified corresponding Uniprot ID as %s." % (tmp_identifier, uniprot))
        return uniprot
    else:
        return None
    
def obtain_pssm(tmp_identifier, sequence, outdir, loggerid):
    '''
    Calculates PSSM with PSIBLAST. This function requires functional PSIBLAST installation and nr database.
    PSIBLAST should be reachable via PATH environment variable.

    :param sequence: Sequence object
    :return: Returns PSSM file name if PSSM calculation is successfully completed, False if calculation is failed.
    '''

    psiblast_exec = "psiblast"
    logger = logging.getLogger(loggerid)

    if not os.path.exists(outdir+'/'+tmp_identifier+'.fasta'):
        tmp = open(outdir+'/'+tmp_identifier+'.fasta', 'w')
        tmp.write(sequence.format('fasta'))
        tmp.close()
    logger.info("Running PSIBLAST for %s..." % sequence.id)

    command = psiblast_exec+" -num_threads %d -db nr -num_iterations 3 -inclusion_ethresh 1e-5 " % MAX_CORE_PER_JOB
    command += "-query "+outdir+"/"+tmp_identifier+".fasta -out_ascii_pssm "+outdir+"/"+tmp_identifier+".pssm"
    logger.debug("Running command: %s" % command)
    args = shlex.split(command)

    psiblast_output = tempfile.NamedTemporaryFile(prefix='psib', suffix='.out', delete=True)
    logger.debug("Writing PSIBLAST output to %s" % psiblast_output.name)
    try:
        process = subprocess.Popen(args, stdout=psiblast_output, stderr=subprocess.PIPE)
    except OSError:
        logger.error("Cannot find psiblast installation. Please make sure you installed BLAST+ and set PATH variable"
                     " correctly.")
        sys.stderr.write("Cannot find psiblast installation. Please make sure you installed BLAST+ and set "
                         "PATH variable correctly.\n")
        psiblast_output.close()
        return False

    for line in process.stderr:
        logger.error(line)

    process.wait()

    logger.info("PSIBLAST run of "+outdir+"/"+tmp_identifier+".fasta finished with code "+str(process.returncode)+".")
    if int(process.returncode) == 0:

        # Remove psiblast output file if job is successful
        psiblast_output.close()

        return tmp_identifier+".pssm"
    else:
        return False

def predict_disorder(tmp_identifier, sequence, outdir, loggerid):
    '''
    Predicts disorder using IUPred. Tries to use local installation first, if fails, tries to use IUPred web server.
    Requires mechanize and BeautifulSoup modules.
    :param sequence: Sequence object
    :return: Returns IUPred prediction result file if prediction is successfully completed, False if prediction is failed.
    '''
    type = 'local'

    logger = logging.getLogger(loggerid)

    if not os.path.exists(outdir+'/'+tmp_identifier+'.fasta'):
        tmp = open(outdir+'/'+tmp_identifier+'.fasta', 'w')
        tmp.write(sequence.format('fasta'))
        tmp.close()

    try:
        iupred = os.environ["IUPred_PATH"]+'/iupred'
    except KeyError:
        logger.info("IUPred environment variable is not set. Trying to access remote server...")
        sys.stderr.write("IUPred environment variable is not set. Trying to access remote server...\n")
        type = 'remote'

    out_file = outdir+'/'+tmp_identifier+'.iupred'
    if not type == 'remote':
        ofh = open(out_file, 'w')
        command = "%s %s long" % (iupred, outdir+'/'+tmp_identifier+'.fasta')
        logger.info("Running IUPred with command: %s" % command)
        logger.info("Expected output file: %s" % out_file)
        args = shlex.split(command)
        process = subprocess.Popen(args, stdout=ofh, stderr=subprocess.PIPE)

        for line in process.stderr:
            logger.error(line)

        process.wait()
        ofh.close()

        logger.info("IUPred run of "+sequence.id+" finished with code "+str(process.returncode)+".")
        if process.returncode != 0:
            logger.error("IUPred run was unsuccessful with return code %s." % str(process.returncode))
            return False
    else:
        try:
            import BeautifulSoup
        except ImportError:
            logger.error("Remote IUPred usage requires BeautifulSoup module, which is not installed in your system.")
            sys.stderr.write("Remote IUPred usage requires BeautifulSoup module, which is not installed in your system.\n")
            return False

        try:
            import mechanize
        except ImportError:
            logger.error("Remote IUPred usage requires mechanize module, which is not installed in your system.")
            sys.stderr.write("Remote IUPred usage requires mechanize module, which is not installed in your system.\n")
            return False

        logger.info("Loading IUPred server for %s." % sequence.identifier)
        response = mechanize.urlopen("http://iupred.enzim.hu/")
        forms = mechanize.ParseResponse(response, backwards_compat=False)
        try:
            form = forms[0]
            form['seq'] = sequence.seq
            form['type'] = ['long']
            form['output'] = ['data']
            request2 = form.click()
            response2 = mechanize.urlopen(request2)
            content = response2.read()
        except Exception, e:
            logger.error("Problem in IUPred remote server! Exception: %s" % str(e))
            return False

        soup = BeautifulSoup.BeautifulSoup(content)
        table = soup.find('table', attrs={'width': 420})

        ofh = open(out_file, 'w')

        rows = table.findAll('tr')
        for i, tr in enumerate(rows):
            if i == 1:
                continue

            row_content = []
            cols = tr.findAll('td')
            for j, td in enumerate(cols):
                text = ''.join(td.find(text=True))
                if (i == 0 and j == 0) and str(text) != "Disorder prediction score":
                    break
                elif i >= 2:
                    row_content.append(str(text.strip()))
            if row_content:
                ofh.write("%s\t%s\t%s\n" % (row_content[0], row_content[1], row_content[2]))
        ofh.close()
        logger.info("Completed prediction for %s, file saved to: %s." % (sequence.id, out_file))

    return out_file

def get_classifier(data, jobid):
    '''
    This function fits a classifier model to the training data. Classifier is trained in the each run to ensure
    compability between different scikit-learn versions. Serialized classifier objects fail to function in different
    versions of scikit-learn.
    :return: classifier and scaler object, as well as metadata of classifier.
    '''
    logger = logging.getLogger('job'+jobid)
    pickle = data
    try:
        train_dataset, metadata = cPickle.load(open(pickle, 'rb'))
    except Exception, e:
        logger.error("Problem in loading pickled dataset. Exception: %s" % str(e))
        return False, False, False

    try:
        scaler = preprocessing.MinMaxScaler().fit(train_dataset.data)
    except Exception, e:
        logger.error("Problem in scaler fitting. Exception: %s" % str(e))
        return False, False, False

    try:
        clf = svm.SVC(C=metadata['C'], kernel='rbf', gamma=metadata['gamma'], class_weight=metadata['class_weight'], probability=True)
        clf.fit(scaler.transform(train_dataset.data), train_dataset.target)
    except Exception, e:
        logger.error("Problem in training the classifier. Exception: %s" % str(e))
        return False, False, False

    return clf, scaler, metadata
