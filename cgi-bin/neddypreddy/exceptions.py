__author__ = 'asyavuz'
class NoPDB(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

class NoFeature(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class InvalidFile(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

class InvalidInterpreter(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)