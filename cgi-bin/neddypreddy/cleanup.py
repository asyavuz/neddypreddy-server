__author__ = 'asyavuz'
import os
import sys
import json
import time
import tools
import shutil
import logging
import datetime
import requests
import mysql.connector

try:
    config = json.load(open('../config.json'))
except Exception, e:
    sys.stderr.write(str(e)+"\n")
    sys.exit(1)

jobsdir = config['server']['jobsDir']

logging.basicConfig(filename=config['server']['logDir']+'/cleaner.log',
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.DEBUG)
mainLogger = logging.getLogger("cleaner")

# Connect mySQL server to fetch and process jobs
mainLogger.info("Trying to connect mySQL server...")
try:
    cnx = mysql.connector.connect(user=config['mysql']['username'], password=config['mysql']['password'],
                                  host=config['mysql']['host'],
                                  database=config['mysql']['database'])
except Exception, e:
    tools.send_email("", config['server']['adminEmail'], 'Incident: mySQL Related Problem! (Cleaner)',
                     '<br /> or <br /><br />Dear Admin,<br />Your server configuration is not working properly at the '
                     'moment. We have received following error: %s. <br /><br />Please fix this problem as soon as '
                     'possible.<br /><br />Sincerely,<br />NeddyPreddy Server' % str(e))
    raise e
cursor = cnx.cursor()

# Lets calculate the day threshold
end_date = datetime.datetime.now() - datetime.timedelta(days=config['server']['keepJobsForDay'])
end_date_fmt = end_date.strftime("%Y-%m-%d %H:%M:%S")

# Give triple time if there's a possibility of job still running (not desired but customer satisfaction is the priority)
end_date2 = datetime.datetime.now() - datetime.timedelta(days=3*config['server']['keepJobsForDay'])
end_date2_fmt = end_date2.strftime("%Y-%m-%d %H:%M:%S")

mainLogger.info("End Date:", end_date_fmt)
mainLogger.info("End Date 2:", end_date2_fmt)

# Get jobs completed or failed via runner
mainLogger.info("Getting jobs to be cleaned from database...")
query = "SELECT jobid FROM queue WHERE finishdate <= '%s' and status != '%s';" % (end_date_fmt, str(3))
cursor.execute(query)

jobs = []
for jobid in cursor:
    jobs.append(jobid[0])
#    print jobid[0]

cursor.close()
mainLogger.info("Found %d jobs to be cleaned up." % len(jobs))

never_finished = []
mainLogger.info("Getting started but never finished jobs to be cleaned from database...")
cursor = cnx.cursor()
query = "SELECT jobid FROM queue WHERE submitdate <= '%s' and finishdate IS NULL and status = '%s';" % (end_date2_fmt, str(1))
cursor.execute(query)

for jobid in cursor:
    jobs.append(jobid[0])
    never_finished.append(jobid[0])

cursor.close()
mainLogger.info("Found %d jobs to be cleaned up." % len(never_finished))

dbclean = len(jobs)
# Get jobs failed before runner
mainLogger.info("Getting jobs to be cleaned from file structure...")
for root, dirs, files in os.walk(jobsdir):
    for name in files:
        if name == "ERROR.txt":
            # Get last modification date of the error.txt. If its recent, lets keep error file. So, if user wants
            # a follow up, or reports to us, we can check the error via logs and input files.

            lm = time.ctime(os.path.getctime(os.path.join(root, name)))
            last_modif = datetime.datetime.strptime(lm, "%a %b %d %H:%M:%S %Y")

            if last_modif <= end_date:
                jobs.append(str(os.path.split(root)[-1]))

mainLogger.info("Found %d jobs to be cleaned up." % (len(jobs)-dbclean))
mainLogger.info("Total Jobs to be Cleaned: %d." % len(jobs))

success = 0
failed = 0
for job in jobs:
    mainLogger.info("Cleaning %s..." % job)
    try:
        for root, dirs, files in os.walk(jobsdir+'/'+job):
            for name in files:
                fst = os.stat(root+'/'+name)
                if fst.st_uid == os.getuid():
                    mainLogger.debug("Trying to remove %s" % jobsdir+job+'/'+name)
                    os.remove(jobsdir+job+'/'+name)
                    mainLogger.debug("Removed %s from %s." % (name, jobsdir+'/'+job))
                    
        r = requests.post(str(config['server']['host'])+"/cgi-bin/clean_apache.py", data={'job': job})
        r_data = json.loads(r.text)
        if r_data['status']=='Success':
            cursor = cnx.cursor()
            if job in never_finished:
                cursor.execute("UPDATE queue SET status=%s WHERE jobid='%s' " % (str(4), job))
            else:
                cursor.execute("UPDATE queue SET status=%s WHERE jobid='%s' " % (str(3), job))
            cnx.commit()
            cursor.close()
        else:
            raise Exception("Failed apache removal!")
        success += 1
    except Exception, e:
        mainLogger.error("Exception while deleting the folder: %s" % str(e))
        failed += 1

mainLogger.info("Successfully deleted %d job directories. Failed to delete %d job directories." % (success, failed))
mainLogger.info("Done for now.")
cnx.close()
