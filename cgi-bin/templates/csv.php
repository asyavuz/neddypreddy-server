<?php
// Make sure we dont' time out, this is optional
@set_time_limit(10000);

$filename = "output.csv";
if (!empty($_POST['filename']) )
	$filename = $_POST['filename'] ;

// NO CACHE
header("Pragma: public");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

// Set headers (force save as dialog)
header("Content-type: text/csv; charset=utf-8");
header("Content-disposition: attachment; filename='" . $filename . "'");

echo $_POST['data'];
?>
