#NeddyPreddy Server
This repository contains the source code of NeddyPreddy. NeddyPreddy is a neddylation site predictor that uses SVM to predict neddylation sites from protein sequences.

You can access to NeddyPreddy at http://neddypreddy.sabanciuniv.edu.

###Licence
NeddyPreddy uses the MIT License (see LICENCE.md). Please open an issue in this page if you have any questions.

###Reference
If you would like to use NeddyPreddy in your publications, please consider citing:

	Yavuz AS, Sozer NB, Sezerman OS. "NeddyPreddy: prediction of neddylation sites from protein sequences", in preparation.